# Statistiques I pour psychologues (2022-2023)

Voici un résumé détaillé de mes notes de cours. Quelques erreurs s'y sont peut-être glissées, mais de manière générale la plupart des choses devrait être juste.

Je ne demande rien en échange, mais si ces notes t'ont été utiles, tu peux m'envoyer un petit message à `notes@enbian.space`, ça me fera toujours plaisir! :D

Si tu as trouvé une erreur, tu peux aussi me l'indiquer à l'adresse ci-dessus.
