# Bibliographie

Le cours est bien sûr la référence principale. Néanmoins, certaines ressources externes se sont révélées utiles à la conception de ce résumé.

## Livres

1. *Psychologie expérimentale*, Hansen, Christine H; Myers, Anne, _De Boeck Sup_, 2007 (ISBN: 9782804155360)

## Sites internet

1. [https://support.minitab.com/fr-fr/minitab/20/help-and-how-to/statistics/basic-statistics/supporting-topics/basics/null-and-alternative-hypotheses/](https://support.minitab.com/fr-fr/minitab/20/help-and-how-to/statistics/basic-statistics/supporting-topics/basics/null-and-alternative-hypotheses/)
1. [https://support.minitab.com/fr-fr/minitab/20/help-and-how-to/statistics/basic-statistics/supporting-topics/basics/what-is-a-confidence-interval/](https://support.minitab.com/fr-fr/minitab/20/help-and-how-to/statistics/basic-statistics/supporting-topics/basics/what-is-a-confidence-interval/)
1. [https://www.studocu.com/fr-ch/document/universite-de-lausanne/statistique-i-pour-psychologues/formules-statistiques-pour-psychologues/7576612](https://www.studocu.com/fr-ch/document/universite-de-lausanne/statistique-i-pour-psychologues/formules-statistiques-pour-psychologues/7576612)
1. [https://www.voxco.com/fr/blog/degres-de-liberte-dans-le-test-t/](https://www.voxco.com/fr/blog/degres-de-liberte-dans-le-test-t/)
1. [https://stats.stackexchange.com/questions/238427/why-the-lower-tail-f-is-used-when-mannualy-calculating-the-p-value-from-t-score](https://stats.stackexchange.com/questions/238427/why-the-lower-tail-f-is-used-when-mannualy-calculating-the-p-value-from-t-score)
1. [https://www.r-tutor.com/elementary-statistics/probability-distributions/binomial-distribution](https://www.r-tutor.com/elementary-statistics/probability-distributions/binomial-distribution)
1. [https://www.geeksforgeeks.org/chi-square-distribution-in-r/](https://www.geeksforgeeks.org/chi-square-distribution-in-r/)
1. [https://www.geo.fu-berlin.de/en/v/soga/Basics-of-statistics/Continous-Random-Variables/Chi-Square-Distribution/Chi-Square-Distribution-in-R/index.html](https://www.geo.fu-berlin.de/en/v/soga/Basics-of-statistics/Continous-Random-Variables/Chi-Square-Distribution/Chi-Square-Distribution-in-R/index.html)
1. [https://datatab.fr/tutorial/wilcoxon-test](https://datatab.fr/tutorial/wilcoxon-test)
1. [https://www.unilim.fr/pages_perso/stephane.vinatier/Biologie/ctd10.pdf](https://www.unilim.fr/pages_perso/stephane.vinatier/Biologie/ctd10.pdf)
1. [https://stat.ethz.ch/R-manual/R-devel/library/stats/html/SignRank.html](https://stat.ethz.ch/R-manual/R-devel/library/stats/html/SignRank.html)
1. [https://www.rdocumentation.org/packages/stats/versions/3.6.2/topics/SignRank](https://www.rdocumentation.org/packages/stats/versions/3.6.2/topics/SignRank)
1. [https://tjmurphy.github.io/jabstb/signrank.html](https://tjmurphy.github.io/jabstb/signrank.html)
1. [https://www.statology.org/mann-whitney-u-test-r/](https://www.statology.org/mann-whitney-u-test-r/)
1. [https://tjmurphy.github.io/jabstb/ranksum.html](https://tjmurphy.github.io/jabstb/ranksum.html)
1. [https://www.statology.org/kruskal-wallis-test/](https://www.statology.org/kruskal-wallis-test/)
