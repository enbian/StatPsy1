# Test de Mann-Whitney

!!! abstract "Introduction"
    Le test de Mann-Whitney est l'équivalent non-paramétrique du [test de Student à deux groupes indépendants](test-student-2-echantillons-independants.md).

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Nos deux échantillons semblent provenir d'une même population. | Nos deux échantillons semblent provenir de populations différentes. |
| μ~1~ = μ~2~ | μ~1~ ≠ μ~2~  |

## Exécution du test

Vérification des conditions d'application:

=== "Avec R"

    ```R
    # Test de normalité de Shapiro-Wilk
    shapiro.test(residuals(lm(X ~ G)))
    ```

Application du test:

Comme pour le test de Wilcoxon, on attribue des rangs aux différentes mesures. Ici, on regarde si ces rangs semblent être distribués de façon équitable entre les deux échantillons.

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche
    wilcox.test(X ~ G, alternative="less")

    # Test unilatéral à droite
    wilcox.test(X ~ G, alternative="greater")

    # Test bilatéral
    wilcox.test(X ~ G, alternative="two.sided")
    ```

=== "Avec R (lent)"

    !!! warning "Attention"

        La méthode lente n'est à utiliser que s'il n'y a pas d'ex-aequo dans les rangs (que deux rangs n'ont pas la même valeur) Pour être plus sûr·e·x, il est conseillé de toujours utiliser la méthode rapide (qui s'occupe de faire les corrections nécessaires au cas où), sauf indication contraire.

    ```R
    R <- rank(abs(X))  # attribution des rangs

    T1 <- sum(R[is.element(X, x1)])  # rangs issus de x1
    T2 <- sum(R[is.element(X, x2)])  # rangs issus de x2

    # valeur empirique de la variable de décision
    U1 <- (n1*n2)+(n1*(n1+1)/2)-T1
    U2 <- (n1*n2)+(n2*(n2+1)/2)-T2

    # Test unilatéral à gauche
    pwilcox(U2, n1, n2, lower.tail=TRUE)

    # Test unilatéral à droite
    pwilcox(U1, n1, n2, lower.tail=TRUE)

    # Test bilatéral
    2*min(
        pwilcox(U2, n1, n2, lower.tail=TRUE),
        pwilcox(U1, n1, n2, lower.tail=TRUE)
    )
    ```

## Exemple

On cherche à savoir si les jeunes adultes ont davantage de crises d'angoisse que les personnes âgées. On mesure donc, sur un mois, le nombre de crises d'angoisse chez des individus de ces deux groupes.

Voici une représentation graphique de nos deux groupes expérimentaux:

<figure markdown>
![Représentation graphique de nos données](test-mann-whitney.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

Importons nos données dans R:

=== "Avec R"

    ```R
    # Jeunes adultes:
    x1 <- c(4, 5, 3, 6, 10)
    # Personnes âgées:
    x2 <- c(1, 2, 7, 0)

    n1 <- length(x1)
    n2 <- length(x2)

    X <- c(x1, x2)
    G <- factor(
        rep(1:2, c(n1, n2)),
        labels=c(
            "Jeunes adultes", "Personnes âgées"
        )
    )
    ```

Vérification des conditions d'application:

=== "Avec R"

    ```R
    shapiro.test(residuals(lm(X ~ G)))
    # W = 0.82006, p-value = 0.03452
    # Les données ne sont pas issues de distributions normales
    ```

Exécution de test:

=== "Avec R"

    ```R
    # Test unilatéral à droite (H1: μ1 > μ2)
    wilcox.test(X ~ G, alternative="greater")
    # W = 16, p-value = 0.09524
    ```

=== "Avec R (lent)"

    ```R
    R <- rank(abs(X))

    T1 <- sum(R[is.element(X, x1)])
    T2 <- sum(R[is.element(X, x2)])

    U1 <- (n1*n2)+(n1*(n1+1)/2)-T1
    U2 <- (n1*n2)+(n2*(n2+1)/2)-T2

    pwilcox(U1, n1, n2, lower.tail=TRUE)
    # [1] 0.0952381
    ```

Nous acceptons donc H~0~ au seuil de 5% et concluons que les jeunes adultes n'ont pas plus de crises d'angoisses que les personnes âgées.
