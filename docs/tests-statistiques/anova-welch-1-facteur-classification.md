# Analyse de variance de Welch

!!! abstract "Introduction"

    L'analyse de variance (ANOVA) de Welch est un test paramétrique que l'on utilise lorsque l'on doit effectuer une ANOVA sur des données dont les variances sont hétérogènes.

## Hypothèses


| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Tous nos échantillons semblent provenir d'une même population. | Tous nos échantillons ne semblent pas provenir d'une seule et même population. |
| μ~1~ = μ~2~ = ... = μ~k~ | μ~1~ ≠ μ~2~ \|\| ... \|\| μ~1~ ≠ μ~k~ \|\| μ~2~ ≠ μ~k~ |

## Exécution du test

!!! success "Conditions d'application"

    Pour pouvoir utiliser une ANOVA de Welch:

    - Si m > 9:
        - Si n~j~ > 20 ou alors si les données sont issues de distributions normales:
            - Si les variances sont hétérogènes, utiliser l'ANOVA de Welch
            - Sinon utiliser [l'ANOVA de Fisher](anova-1-facteur-classification.md)
        - Sinon, utiliser [un test de Kruskal-Wallis](test-kruskal-wallis.md)
    - Sinon:
        - Si n~j~ > 15 ou alors si les données sont issues de distributions normales:
            - Si les variances sont hétérogènes, utiliser l'ANOVA de Welch
            - Sinon utiliser [l'ANOVA de Fisher](anova-1-facteur-classification.md)
        - Sinon, utiliser [un test de Kruskal-Wallis](test-kruskal-wallis.md)

=== "Avec R (rapide)"

    ```R
    # ANOVA (Welch)
    oneway.test(X ~ G)
    ```

## Comparaisons multiples

Comme pour l'ANOVA de Fisher, nous devons également comparer nos échantillons pour savoir où se trouvent les différences significatives:

=== "Avec R"

    ```R
    # Comparaisons multiples
    pairwise.t.test(x=X, g=G, p.adjust.method="holm", pool.sd=FALSE)
    ```

    !!! note

        La méthode d'ajustement de `holm` est la plus précise et donc celle qui est recommandée. Cependant, il est aussi possible de ne pas effectuer de correction et d'utiliser `none` à la place, ou alors d'utiliser la méthode de `bonferroni`.


## Exemple

Nous souhaitons tester s'il y a un lien entre classe sociale et nombre d'années d'études.

Hypothèses:

- H~0~ : il n'y a aucun lien entre classe sociale et nombre d'années d'études
- H~1~ : il y a un lien entre classe sociale et nombre d'années d'études

Importons nos données dans R:

=== "Avec R"

    ```R
    x1 <- c(11, 10, 12, 11, 11, 11, 9, 12)
    x2 <- c(15, 14, 16, 15, 14, 11)
    x3 <- c(16, 20, 25, 18, 19, 22, 18, 25, 16)

    n1 <- length(x1)
    n2 <- length(x2)
    n3 <- length(x3)

    X <- c(x1, x2, x3)
    G <- factor(
        rep(1:3, c(n1, n2, n3)),
        labels=c(
            "Précaire", "Moyenne", "Supérieure"
        )
    )
    ```

Vérification des conditions d'application:

=== "Avec R"

    ```R
    # m = 3 < 9

    # n1 = 8, n2 = 6, n3 = 9
    # nj < 15

    # Données sont-elles issues de distributions normales?
    shapiro.test(residuals(lm(X ~ G)))
    # W = 0.93511, p-value = 0.141
    # acceptation de H0 -> distributions sont normales

    # Variances homogènes?
    leveneTest(X ~ G, center=median)
    # Pr(>F) = 0.03851  -> rejet de H0
    # Les variances ne sont pas homogènes
    ```

Puisque les conditions d'application sont remplies, nous pouvons effectuer notre ANOVA de Welch:

=== "Avec R (rapide)"

    ```R
    oneway.test(X ~ G)
    # F = 31.477, num df = 2.000, denom df = 10.653, p-value = 3.378e-05
    ```

Puisque p < 0.05, nous rejettons H~0~ et concluons qu'il y a une différence significative entre nos échantillons. Il ne reste qu'à déterminer où se situent ces différences:

=== "Avec R"

    ```R
    pairwise.t.test(x=X, g=G, p.adjust.method="holm", pool.sd=FALSE)
    ```

|  | Précaire | Moyenne |
| --- | --- | --- |
| **Moyenne** | 0.0035 | - |
| **Supérieure** | 8.3e-05 | 0.0021 |

Nous constatons, qu'il y a, au seuil de 5%, une différence significative entre tous les groupes expérimentaux.
