# Test de Student sur un échantillon

!!! abstract "Introduction"
    Le t-test de Student sur un échantillon est un test paramétrique nous permettant de déterminer s'il y existe une différence significative entre 
    les scores d'un échantillon par rapport à la population dont est issu cet échantillon.

## Variables

- μ : moyenne de la population dont est issu notre échantillon
- μ~0~ : moyenne de la population dont devrait être issu notre échantillon
- σ^2^ : variance de la population
- n : effectif de l'échantillon
- x̅ : moyenne de l'échantillon
- s^2^ : variance (*écart-type^2^*) de l'échantillon

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Notre échantillon semble être issu de la population dont il devrait être issu. | Notre échantillon ne semble pas être issu de la population dont il devrait être issu. |
| μ = μ~0~ | μ ≠ μ~0~  |

## Exécution du test

!!! success "Conditions d'application"

    Pour que le test puisse être utilisé:
    
    - Si `n <= 20` : les données doivent **être issues d'une distribution normale**
    - Si `n > 20` : il ne doit **pas y avoir de valeur extrêmes**

    Si ces conditions ne sont pas remplies, nous devons utiliser un test non-paramétrique à la place (moins puissant). Dans ce cas, il s'agit d'un test de Wilcoxon sur un échantillon.

Vérification des conditions d'application:

=== "Avec R"

    ```R
    n <- length(x)  # effectif de l'échantillon

    # Si n <= 20:
    # Test de normalité de Shapiro-Wilk
    shapiro.test(x)
    # Si la p-value de ce test est inférieure à 0.05 (seuil alpha),
    # cela signifie que la distribution parente n'est pas normale

    # Si n > 20:
    # Mise en évidence des données extrêmes
    outlier <- abs(x - median(x)) > 3*mad(x, constant = 1)
    x[outlier]
    # Si le résultat est 0 => pas de données extrêmes
    ```

Avec:

- t~emp~ : la valeur empirique de la variable de décision = [(x̅ - μ~0~) / s] * sqrt(n)

Comme pour le test du khi-carré, il s'agit ici de regarder si t~emp~ se trouve dans une des zones critiques de la distribution de Student.
Voici une représentation d'une distribution de Student à 10 degrés de liberté:


<figure markdown>
![Distribution de Student à 10 degrés de liberté](test-student-1-echantillon.png)
<ficaption>Distribution de Student à 10 degrés de liberté</ficaption>
</figure>

???+ example "Pour aller plus loin..."

    Nous pouvons également représenter les zones critiques en fonction de la forme de test que nous souhaitons utiliser:

    === "Test unilatéral à gauche"

        <figure markdown>
        ![Distribution de Student à 10 degrés de liberté](test-student-1-echantillon-3.png)
        <ficaption>Distribution de Student à 10 degrés de liberté</ficaption>
        </figure>

    === "Test unilatéral à droite"

        <figure markdown>
        ![Distribution de Student à 10 degrés de liberté](test-student-1-echantillon-2.png)
        <ficaption>Distribution de Student à 10 degrés de liberté</ficaption>
        </figure>

    === "Test bilatéral"

        <figure markdown>
        ![Distribution de Student à 10 degrés de liberté](test-student-1-echantillon-4.png)
        <ficaption>Distribution de Student à 10 degrés de liberté</ficaption>
        </figure>

Puis, nous pouvons effectuer notre test:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche
    t.test(x, mu=mu_0, alternative = "less")

    # Test unilatéral à droite
    t.test(x, mu=mu_0, alternative = "greater")

    # Test bilatéral
    t.test(x, mu=mu_0, alternative = "two.sided")
    ```

=== "Avec R (lent)"

    ```R
    x.bar <- mean(x)  # moyenne de l'échantillon
    s <- sd(x)  # écart-type de l'échantillon
    t.emp <- (x.bar - mu_0)/(s/sqrt(n))  # valeur empirique de la variable de décision

    # Test unilatéral à gauche
    pt(t.emp, n-1, lower.tail=TRUE)

    # Test unilatéral à droite
    pt(t.emp, n-1, lower.tail=FALSE)

    # Test bilatéral
    2*min(
        pt(t.emp, n-1, lower.tail=FALSE), 
        pt(t.emp, n-1, lower.tail=TRUE)
    )
    ```

=== "Avec R (très lent)"

    ```R
    x.bar <- mean(x)  # moyenne de l'échantillon
    s <- sd(x)  # écart-type de l'échantillon
    t.emp <- (x.bar - mu_0)/(s/sqrt(n))  # valeur empirique de la variable de décision

    # On détermine la/les valeur(s) critique(s)
    # et on regarde si t empirique est dans la 
    # ou les zone(s) critique(s)

    # Test unilatéral à gauche
    t.emp >= qt(1 - 0.05, n-1, lower.tail=FALSE)
    
    # Test unilatéral à droite
    t.emp <= qt(1 - 0.05, n-1, lower.tail=TRUE)

    # Test bilatéral
    t.emp >= qt(1 - 0.025, n-1, lower.tail=FALSE)
    t.emp <= qt(1 - 0.025, n-1, lower.tail=TRUE)
    ```

## Exemple

Sur une bouteille de boisson d'un litre, il est écrit que l'on trouve 20g de sucre par litre. Néanmoins, il semblerait que ce taux est inférieur à la réalité. Afin de déterminer si cette affirmation est correcte, nous effectuons des mesures sur 30 bouteilles.

Nous pouvons poser nos hypothèses:

- H~0~ : il y a en effet 20g de sucre par litre
- H~1~ : il y a plus que 20g de sucre par litre

Nous pouvons représenter nos mesures graphiquement:

<figure markdown>
![Représentation graphique de nos données](test-student-1-echantillon-5.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

Saisissons nos données dans R:

=== "Avec R"

    ```R
    mu_0 <- 20  # population dont devrait être issu notre échantillon
    x <- c(
        20, 23, 19, 22, 21, 20, 20, 18, 23, 20,
        18, 24, 23, 20, 19, 22, 19, 23, 22, 22,
        20, 21, 21, 21, 24, 20, 19, 22, 20, 23
    )  # notre échantillon
    n <- length(x)  # effectifs de notre échantillon
    ```

Puisque `n = 30 > 20`, nous vérifions s'il existe des valeurs extrêmes:

=== "Avec R"

    ```R
    # Mise en évidence des données extrêmes
    outlier <- abs(x - median(x)) > 3*mad(x, constant = 1)
    x[outlier]
    ```

Puisqu'il n'y a pas de données extrêmes, nous pouvons réaliser un test de Student **unilatéral à droite**:

=== "Avec R (rapide)"

    ```R
    t.test(x, mu=mu_0, alternative = "greater")
    # t = 3.0932, df = 29, p-value = 0.002176
    ```

=== "Avec R (lent)"

    ```R
    x.bar <- mean(x)  # moyenne de l'échantillon
    s <- sd(x)  # écart-type de l'échantillon    
    t.emp <- (x.bar - mu_0)/(s/sqrt(n))  # valeur empirique de la variable de décision

    pt(t.emp, n-1, lower.tail=FALSE)
    # [1] 0.002175975
    ```

=== "Avec R (très lent)"

    ```R
    x.bar <- mean(x)  # moyenne de l'échantillon
    s <- sd(x)  # écart-type de l'échantillon
    t.emp <- (x.bar - mu_0)/(s/sqrt(n))  # valeur empirique de la variable de décision
    t.crit <- qt(1 - 0.05, n-1, lower.tail=TRUE)  # valeur critique

    t.emp
    # [1] 3.093229
    t.crit
    # [1] 1.699127
    t.emp <= t.crit
    # [1] FALSE
    ```

Nous rejettons donc H~0~ au seuil de 5% et concluons que l'étiquette de la bouteille est mensongère.
