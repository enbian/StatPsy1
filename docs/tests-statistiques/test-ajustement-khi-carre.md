# Test d'ajustement du khi-carré

!!! abstract "Introduction"
    Le test d'ajustement du khi-carré est un test statistique nous permettant de tester s'il existe une différence statistiquement significative entre la distribution des effectifs au sein d'un même échantillon.

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Aucune différence entre les distributions des effectifs n'est pas significative. | Au moins une différence entre les distributions des effectifs est significative. |
| π~1~ = π~1~^0^ \^ π~2~ = π~2~^0^ \^ ... \^ π~m~ = π~m~^0^ | π~1~ ≠ π~1~^0^ \|\| π~2~ ≠ π~2~^0^ \|\| ... \|\| π~m~ ≠ π~m~^0^ |
| χ^2^~emp~ <= χ^2^~crit~ | χ^2^~emp~ > χ^2^~crit~ |
| p >= α | p < α |

## Tableaux des effectifs

Dès lors que nous avons collecté nos données, nous pouvons dresser le tableau des effectifs observés (avec m = nombre de cellules et n~j~ = nombre d'effectif dans une cellule j):

| 1 | 2 | ... | m |
| --- | --- | --- | --- |
| n~1~ | n~2~ | ... | n~m~ |

Ainsi que le tableau des effectifs théoriques attendus sous H~0~ (avec e~j~ = nombre d'effectif dans une cellule j):

| 1 | 2 | ... | m |
| --- | --- | --- | --- |
| e~1~ = nπ~1~^0^ | e~2~ = nπ~2~^0^ | ... | e~m~ = nπ~m~^0^ |

!!! success "Condition d'application"

    Pour que le test puisse être utilisé, la proportion des cellules ayant un effectif théorique inférieur ou égal à 5 ne devrait pas dépasser 20%.

## Variables

- n : somme de tous les effectifs observés (somme de tous les n~j~)
- e : somme de tous les effectifs théoriques (somme de tous les e~j~)
- χ^2^~emp~ : la valeur empirique variable de décision
- χ^2^~crit~ : la valeur critique variable de décision

## Exécution du test

Voici la représentation graphique de la distribution du khi-carré:

<figure markdown>
![Distribution du χ^2^ à 10 degrés de liberté (zone critique en rouge)](test-ajustement-khi-carre.png)
<ficaption>Distribution du χ^2^ à 10 degrés de liberté (zone critique en rouge)</ficaption>
</figure>

Le test consiste à regarder si la valeur empirique de la variable de décision est plus petite que la zone critique (i.e. les 5% les plus à droite de la distribution du khi-carré - pour autant que notre seuil α = 5%).

Il s'agit donc de vérifier si χ^2^~emp~ se trouve à gauche de χ^2^~crit~ (c'est-à-dire à gauche de la zone critique, χ^2^~crit~ étant la limite de cette zone). Si notre variable de décision se trouve dans la zone critique (χ^2^~emp~ > χ^2^~crit~), cela signifie que `p < 0.05` et qu'il faut par conséquent rejetter H~0~.

Par exemple, si χ^2^~emp~ = 20, nous savons dors et déjà que `p < 0.05` car nous pouvons le voir graphiquement (puisque χ^2^~emp~ = 20 > χ^2^~crit~ = 18.307).
Si nous effectuons le test, nous constatons bel et bien que `p = 0.029` et qu'il faut donc rejetter H~0~ au seuil de 5%.

=== "Avec R (rapide)"

    ```R
    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques

    chisq.test(nj, p=pj)
    ```

=== "Avec R (lent)"

    ```R
    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques
    ej <- n*pj  # Somme des effectifs théoriques
    khi2.emp <- sum((nj - ej)^2/ej)  # Valeur empirique de la variable de décision

    # Test unilatéral à droite puisqu'on cherche à savoir si 
    # khi2.emp se trouve à gauche du seuil alpha
    pchisq(khi2.emp, df=m-1, lower.tail=FALSE)
    ```

=== "Avec R (très lent)"

    ```R
    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques
    ej <- n*pj  # Somme des effectifs théoriques
    khi2.emp <- sum((nj - ej)^2/ej)  # Valeur empirique de la variable de décision
    khi2.crit <- qchisq(1 - 0.05, df=m-1)  # Valeur critique de la variable de décision

    # On peut aussi regarder comme ça (procédure plus ancienne):
    khi2.emp <= khi2.crit
    ```

## Exemple

On souhaite tester (au seuil de 5%) si le résultat d'un lancer de dé à 6 faces est dû au hasard. On effectue 60 lancers dont on note les résultats. 

Nos hypothèses sont les suivantes:

- H0: les résultats du lancer de dé sont aléatoires
- H1: les résultats du lancer de dé sont truqués

On peut dès maintenant dresser le tableau des effectifs théoriques auxquels on s'attend sous H~0~ (c'est à dire si le lancer de dé produit un résultat aléatoire):

| 1 | 2 | 3 | 4 | 5 | 6 |
| --- | --- | --- | --- | --- | --- |
| 60/6 = 10 | 60/6 = 10 | 60/6 = 10 | 60/6 = 10 | 60/6 = 10 | 60/6 = 10 |
| 16.667% | 16.667% | 16.667% | 16.667% | 16.667% | 16.667% |

!!! note
    La ligne du haut dans ce tableau correspond aux faces du dé obtenue lors des différents lancers, tandis que la seconde correspond au nombre de lancers lors desquels la face inscrite dans la ligne du haut a été obtenue. La dernière ligne, quant à elle, représente la probabilité théorique de chaque cellule.

Puis, l'on dresse le tableau des effectifs observés lors de nos mesures:

| 1 | 2 | 3 | 4 | 5 | 6 |
| --- | --- | --- | --- | --- | --- |
| 11 | 12 | 7 | 9 | 10 | 11 |

Nous pouvons également représenter la distribution des effectifs observés à l'aide d'un diagramme:

<figure markdown>
![Distribution des effectifs observés](test-ajustement-khi-carre-2.png)
<ficaption>Distribution des effectifs observés</ficaption>
</figure>

Puisque nous avons 6 échantillons, la distribution du χ^2^ va prendre 5 degrés de liberté puisque `df=m-1`.
La distribution du khi-carré à 5 degrés de liberté est la suivante:

<figure markdown>
![Distribution du khi-carré à 5 degrés de liberté](test-ajustement-khi-carre-3.png)
<ficaption>Distribution du khi-carré à 5 degrés de liberté</ficaption>
</figure>

Nous pouvons dès maintenant effectuer notre test:

=== "Avec R (rapide)"

    ```R
    nj <- c(11, 12, 7, 9, 10, 11)

    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques

    x <- chisq.test(nj, p=pj)
    print(x$p.value, 3)
    # [1] 0.901
    ```

=== "Avec R (lent)"

    ```R
    nj <- c(11, 12, 7, 9, 10, 11)

    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques
    ej <- n*pj  # Somme des effectifs théoriques
    khi2.emp <- sum((nj - ej)^2/ej)  # Valeur empirique de la variable de décision
    khi2.crit <- qchisq(1 - 0.05, df=m-1)  # Valeur critique de la variable de décision

    p <- pchisq(khi2.emp, df=m-1, lower.tail=FALSE)
    print(p, 3)
    # [1] 0.901
    ```

=== "Avec R (très lent)"

    ```R
    nj <- c(11, 12, 7, 9, 10, 11)

    n <- sum(nj)  # Somme des effectifs observés
    m <- length(nj)  # Nombre de cellules
    pj <- rep(1/m, m)  # Probabilités théoriques
    ej <- n*pj  # Somme des effectifs théoriques
    khi2.emp <- sum((nj - ej)^2/ej)  # Valeur empirique de la variable de décision
    khi2.crit <- qchisq(1 - 0.05, df=m-1)  # Valeur critique de la variable de décision

    # Pprocédure plus ancienne:
    khi2.emp
    # [1] 1.6
    khi2.crit
    # [1] 11.0705
    khi2.emp <= khi2.crit
    # [1] TRUE
    ```

Comme la probabilité critique p = 0.901 et que `p=0.901 > α=0.05`, nous ne pouvons pas rejetter H~0~. Nous concluons donc que le résultat du lancer de dé est bel et bien aléatoire.
