# Test binomial

!!! abstract "Introduction"
    Un test binomial (également appelé *test sur une proportion*) est un test statistique que l'on utilise pour déterminer la significativité statistique de la différence entre une distribution des succès observés et la distribution théorique.

## Variables

- n = le nombre d'observations
- x = la variable de décision (dans ce cas il s'agit du nombre de succès)
- π~0~ = la probabilité de succès théorique attendue sous H~0~

## Hypothèses

Soit π la probabilité de succès associée à la valeur empirique de la variable de décision. Celle-ci est pour le moment inconnue, mais elle nous permet de définir les deux hypothèses associées à notre test.

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| La différence entre les deux distributions n'est pas significative, le succès est donc dû au hasard. | La différence entre les deux distributions est significative, le succès n'est donc pas dû au hasard. |
| π = π~0~ | π < π~0~ \|\| π > π~0~ \|\| π ≠ π~0~  |


## Formes de test

En fonction de ce que nous recherchons, nous devons utiliser une forme de test bien précise. Cette forme dépend de notre hypothèse alternative (H~1~).

| Test unilatéral à gauche | Test unilatéral à droite | Test bilatéral |
| --- | --- | --- |
| On cherche à tester si la probabilité de succès associée à la valeur empirique de notre variable de décision est **plus faible** que la probabilité théorique. | On cherche à tester si la probabilité de succès associée à la valeur empirique de notre variable de décision est **plus grande** que la probabilité théorique.  | On cherche à tester si la probabilité de succès associée à la valeur empirique de notre variable de décision est **différente** de la probabilité théorique. |
| H~1~: π < π~0~ | H~1~: π > π~0~ | H~1~: π ≠ π~0~ |

## Exécution du test

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche (H1: π < π0)
    binom.test(x=x, n=n, p=pi_0, alternative="less")

    # Test unilatéral à droite (H1: π > π0)
    binom.test(x=x, n=n, p=pi_0, alternative="greater")

    # Test bilatéral (H1: π ≠ π0)
    binom.test(x=x, n=n, p=pi_0, alternative="two.sided")
    ```

=== "Avec R (lent)"

    ```R
    # Test unilatéral à gauche (H1: π < π0)
    pbinom(x, size=n, prob=pi_0, lower.tail=TRUE)

    # Test unilatéral à droite (H1: π > π0)
    pbinom(x-1, size=n, prob=pi_0, lower.tail=FALSE)

    # Test bilatéral (H1: π ≠ π0)
    2*min(
        pbinom(x, size=n, prob=pi_0, lower.tail=TRUE),
        pbinom(x-1, size=n, prob=pi_0, lower.tail=FALSE)
    )
    ```

La probabilité critique p issue du test permettra d'émettre des inférences quant au hasard de succès.

## Interprétation et seuil de significativité

Nous pouvons maintenant fixer le seuil de significativité:

- α = seuil de significativité (probabilité de rejetter ou d'accepter H~0~ à tort). Par convention, ce seuil est généralement fixé à 5%.

!!! failure "Erreur de 1^e^ et 2^e^ espèce"
    Lorsque H~0~ est rejettée à tort, il s'agit d'une **erreur de première espèce**. À l'inverse, lorsque H~0~ est acceptée à tort, il s'agit d'une **erreur de seconde espèce**. C'est pour cette raison que, plus le seuil est petit, plus le risque d'erreur **de première espèce** est faible.


Nous pouvons donc fixer la règle de décision suivante:

| Si p < α | Si p >= α |
| --- | --- |
| :material-close-circle: Rejet de H~0~ (en faveur de H~1~) | :material-check-circle: Acceptation de H~0~ |

!!! warning "Important"
    Cette règle reste toujours la même, indépendamment de la forme de test utilisée.
    La variable α correspond au seuil sous lequel H~0~ est rejettée.

## Exemple

On souhaite tester si le résultat d'un lancer d'une pièce à pile ou face est dû au hasard. Pour cela, on effectue dix lancers dont on note le résultat. 

Nos hypothèses sont les suivantes:

- H~0~: les résultats du lancer de pièce sont aléatoires
- H~1~: les résultats du lancer de pièce sont truqués

Si H~0~ est vraie, on devrait obtenir 5 piles et 5 faces (soit une proportion de `1/2`). On peut donc poser:

- n = 10
- π~0~ = 1/2

Ensuite, on attribue à la variable de décision sa valeur empirique:

- x = 4

Ainsi, si l'on veut représenter graphiquement la distribution de x sous H~0~ (c'est à dire la probabilité que la variable de décision x ait une certaine valeur lorsque H~0~ est vraie), nous obtiendrons le graphique suivant:

<figure markdown>
![Distribution sous H~0~](test-binomial.png)
<ficaption>Distribution théorique de x sous H~0~</ficaption>
</figure>

Grâce à ce graphique, nous pouvons voir que, lorsque H~0~ est vraie, la probabilité que x=4 est d'environ 20.5%.

Maintenant, nous voulons savoir si l'hypothèse nulle (H~0~) peut être rejettée. Comme nous n'avons par d'information sur le seuil de significativité, nous fixons ce dernier à 5% par convention:

- α = 5% = 0.05

Puis, en utilisant R, nous effectuons un test statistique **bilatéral**, puisque nous cherchons à savoir si π ≠ π~0~:

=== "Avec R (rapide)"

    ```R
    x <- 4  # Variable de décision
    n <- 10  # Nombre d'observations
    pi_0 <- 1/2  # Proportion théorique

    # Test bilatéral (H1: π ≠ π0)
    p <- binom.test(x=x, n=n, p=pi_0, alternative="two.sided")
    round(p$p.value, 3)
    # [1] 0.754
    ```

=== "Avec R (lent)"

    ```R
    x <- 4  # Variable de décision
    n <- 10  # Nombre d'observations
    pi_0 <- 1/2  # Proportion théorique

    # Test bilatéral (H1: π ≠ π0)
    p <- 2*min(
        pbinom(x, size=n, prob=pi_0, lower.tail=TRUE),
        pbinom(x-1, size=n, prob=pi_0, lower.tail=FALSE)
    )
    round(p, 3)
    # [1] 0.754
    ```

Le résultat de ce test nous donne la probabilité critique p. Dans ce cas précis, p=0.754. Puisque `p=0.754 > α=0.05`, nous ne pouvons pas rejetter H~0~. Par conséquent, nous concluons que le phénomène observé est dû au hasard (ou du moins nous ne pouvons pas prouver le contraire).

!!! note
    Dans ce cas précis, puisque le seuil de significativité α est fixé à 5%, cela signifie que, si l'on répète cette expérience 100 fois à chaque fois avec de nouvelles mesures, il y aura en moyenne 5 cas dans lesquels H~0~ sera rejettée à tort (erreur de première espèce).
