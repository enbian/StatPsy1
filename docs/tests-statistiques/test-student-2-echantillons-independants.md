# Test de Student sur deux échantillons indépendants

!!! abstract "Introduction"

    Le t-test de Student sur deux groupes indépendants est un test paramétrique nous permettant de déterminer si deux échantillon proviennent de la même population ou non. Il peut notamment être utilisé pour comparer les scores d'un groupe contrôle avec les scores du groupe expérimental.

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Nos deux échantillons semblent provenir d'une même population. | Nos deux échantillons semblent provenir de populations différentes. |
| μ~1~ = μ~2~ | μ~1~ ≠ μ~2~  |

## Exécution du test

!!! success "Conditions d'application"

    Pour que le test puisse être utilisé:
    
    - Si n~1~ > 15 et n~2~ > 15 :
        - Si les variances sont homogènes, nous pouvons utiliser le test de Student
        - Sinon, utiliser un [test de Welch](test-welch-2-echantillons-independants.md)
    - Sinon :
        - Si les données ne sont pas issues de distributions normales, nous devons utiliser un test non-paramétrique (dans ce cas un [test de Mann-Whitney]())
        - Si les variances sont homogènes, nous pouvons utiliser le test de Student
        - Sinon, utiliser un [test de Welch](test-welch-2-echantillons-independants.md)

Vérification des conditions d'application:

=== "Avec R"

    ```R
    library(car)
    library(psych)

    # Statistique descriptive
    by(X, G, describe, skew=FALSE, range=FALSE)

    # Données sont-elles issues de distributions normales?
    # Avec H0: distributions sont normales
    shapiro.test(residuals(lm(X ~ G)))

    # Test de l'homogénéité des variances
    # Avec H0: variances sont homogènes
    leveneTest(X ~ G, center=median)
    ```

Application du test:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche
    t.test(X ~ G, var.equal=TRUE, alternative="less")

    # Test unilatéral à droite
    t.test(X ~ G, var.equal=TRUE, alternative="greater")

    # Test bilatéral
    t.test(X ~ G, var.equal=TRUE, alternative="two.sided")
    ```

=== "Avec R (lent)"

    ```R
    # Estimation de la variance commune
    sigma2 <- ((n1 - 1)*sd(x1)^2 + (n2 - 1)*sd(x2)^2)/(n1 + n2 - 2)

    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((1/n1) + (1/n2))*sigma2)

    # Test unilatéral à gauche
    pt(t.emp, n1+n2-2, lower.tail=TRUE)

    # Test unilatéral à droite
    pt(t.emp, n1+n2-2, lower.tail=FALSE)

    # Test bilatéral
    2*min(
        pt(t.emp, n1+n2-2, lower.tail=FALSE), 
        pt(t.emp, n1+n2-2, lower.tail=TRUE)
    )
    ```

=== "Avec R (très lent)"

    ```R
    # Estimation de la variance commune
    sigma2 <- ((n1 - 1)*sd(x1)^2 + (n2 - 1)*sd(x2)^2)/(n1 + n2 - 2)

    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((1/n1) + (1/n2))*sigma2)

    # Test unilatéral à gauche
    t.emp >= qt(1 - 0.05, n1+n2-2, lower.tail=FALSE)

    # Test unilatéral à droite
    t.emp <= qt(1 - 0.05, n1+n2-2, lower.tail=TRUE)

    # Test bilatéral
    t.emp >= qt(1 - 0.025, n1+n2-2, lower.tail=FALSE)
    t.emp <= qt(1 - 0.025, n1+n2-2, lower.tail=TRUE)

    # Le résultat nous dit si on peut accepter H0 ou pas
    # TRUE -> acceptation de H0
    # FALSE -> rejet de H0
    ```

## Taille d'effet

Pour mesurer la taille de l'effet, on utilise le coefficient d de Cohen:

=== "Avec R"

    ```R
    library(effectsize)

    # Coefficient d de Cohen
    d <- cohens_d(X ~ G, pooled_sd=TRUE)
    interpret_cohens_d(d)
    ```

## Exemple

On cherche à tester si écouter de la musique augmente les performances intellectuelles.
Pour cela, on fait passer un test à deux groupes: le premier réalise l'épreuve sans musique, alors que le second groupe doit effectuer la même tâche mais avec de la musique. On mesure la vitesse (en minutes) avec laquelle les participant·e·x·s terminent l'exercice.

Posons déjà nos hypothèses:

- H~0~ : la musique n'a pas d'effet sur les performances intellectuelles (μ~1~ = μ~2~)
- H~1~ : la musique améliore les performances intellectuelles (μ~1~ > μ~2~)

Nous pouvons représenter nos données graphiquement:

<figure markdown>
![Représentation graphique de nos données](test-student-2-echantillons-independants.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

Nous pouvons dès lors importer nos données dans R:

=== "Avec R"

    ```R
    x1 <- c(20, 20, 22, 17, 19, 20, 21, 21, 18, 20, 19, 17, 18, 22, 18, 20, 20)
    x2 <- c(17, 16, 19, 16, 15, 18, 18, 17, 15, 17, 18, 15, 15, 19, 16, 17)

    n1 <- length(x1)
    n2 <- length(x2)

    X <- c(x1, x2)
    G <- factor(
        rep(1:2, c(n1, n2)),
        labels=c("Sans musique", "Avec musique")
    )
    ```

Maintenant, vérifions les conditions d'application:

=== "Avec R"

    ```R
    n1
    # [1] 17
    n2
    # [1] 16

    # n1 > 15 et n2 > 15

    # Test de l'homogénéité des variances
    # Avec H0: variances sont homogènes
    leveneTest(X ~ G, center=median)
    # Pr(>F) = 0.877923
    # Acceptation de H0 -> les variances sont homogènes
    ```

Puisque les variances sont homogènes, nous pouvons donc réaliser un test de Student **unilatéral à droite**, puisque notre hypothèse alternative est que μ~1~ > μ~2~:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à droite (H1: μ1 > μ2)
    t.test(X ~ G, var.equal=TRUE, alternative="greater")
    # t = 5.4183, df = 31, p-value = 3.236e-06
    ```

=== "Avec R (lent)"

    ```R
    # Estimation de la variance commune
    sigma2 <- ((n1 - 1)*sd(x1)^2 + (n2 - 1)*sd(x2)^2)/(n1 + n2 - 2)

    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((1/n1) + (1/n2))*sigma2)
    t.emp
    # [1] 5.418294

    # Test unilatéral à droite
    pt(t.emp, n1+n2-2, lower.tail=FALSE)
    # [1] 3.235755e-06
    ```

=== "Avec R (très lent)"

    ```R
    # Estimation de la variance commune
    sigma2 <- ((n1 - 1)*sd(x1)^2 + (n2 - 1)*sd(x2)^2)/(n1 + n2 - 2)

    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((1/n1) + (1/n2))*sigma2)
    t.emp
    # [1] 5.418294

    # Test unilatéral à droite
    t.crit <- qt(1 - 0.05, n1+n2-2, lower.tail=TRUE)
    t.emp <= t.crit
    # [1] FALSE
    ```

Nous concluons donc que la musique a un impact sur les performances intellectuelles au seuil de 5%. Néanmoins, il nous reste à calculer la taille de l'effet, qui nous permet de savoir à quel point cet impact est important:

=== "Avec R"

    ```R
    library(effectsize)

    # Coefficient d de Cohen
    d <- cohens_d(X ~ G, pooled_sd=TRUE)
    interpret_cohens_d(d)

    # Cohen's d |       95% CI | Interpretation
    # -----------------------------------------
    # 1.89      | [1.05, 2.70] |          large
    # 
    # - Estimated using pooled SD.
    # - Interpretation rule: cohen1988
    ```

La taille d'effet est donc **grande**.
