# Test de Welch sur deux échantillons indépendants

!!! abstract "Introduction"

    Le test de Welch est le test paramétrique que l'on utilise lorsque l'on souhaite effectuer un t-test de Student sur des données, mais que ces dernières de sont pas homogènes.

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Nos deux échantillons semblent provenir d'une même population. | Nos deux échantillons semblent provenir de populations différentes. |
| μ~1~ = μ~2~ | μ~1~ ≠ μ~2~  |

## Exécution du test

!!! success "Conditions d'application"

    Pour que le test puisse être utilisé:
    
    - Si n~1~ > 15 et n~2~ > 15 :
        - Si les variances **ne sont pas** homogènes, nous pouvons utiliser le test de Welch
        - Sinon, utiliser un [test de Student](test-student-2-echantillons-independants.md)
    - Sinon :
        - Si les données ne sont pas issues de distributions normales, nous devons utiliser un test non-paramétrique (dans ce cas un [test de Mann-Whitney]())
        - Si les variances **ne sont pas** homogènes, nous pouvons utiliser le test de Welch
        - Sinon, utiliser un [test de Student](test-student-2-echantillons-independants.md)

Vérification des conditions d'application:

=== "Avec R"

    ```R
    library(car)
    library(psych)

    # Statistique descriptive
    by(X, G, describe, skew=FALSE, range=FALSE)

    # Données sont-elles issues de distributions normales?
    # Avec H0: distributions sont normales
    shapiro.test(residuals(lm(X ~ G)))

    # Test de l'homogénéité des variances
    # Avec H0: variances sont homogènes
    leveneTest(X ~ G, center=median)
    ```

Application du test:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche
    t.test(X ~ G, var.equal=FALSE, alternative="less")

    # Test unilatéral à droite
    t.test(X ~ G, var.equal=FALSE, alternative="greater")

    # Test bilatéral
    t.test(X ~ G, var.equal=FALSE, alternative="two.sided")
    ```

=== "Avec R (lent)"

    ```R
    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((sd(x1)^2/n1) + (sd(x2)^2/n2)))

    # Estimation des variances (écart-types au carré)
    s2.1 <- sd(x1)^2
    s2.2 <- sd(x2)^2

    # Degrés de liberté
    v <- (s2.1/n1 + s2.2/n2)^2 /(((s2.1/n1)^2/(n1 - 1)) + ((s2.2/n2)^2/(n2 - 1)))

    # Test unilatéral à gauche
    pt(t.emp, v, lower.tail=TRUE)

    # Test unilatéral à droite
    pt(t.emp, v, lower.tail=FALSE)

    # Test bilatéral
    2*min(
        pt(t.emp, v, lower.tail=FALSE), 
        pt(t.emp, v, lower.tail=TRUE)
    )
    ```

=== "Avec R (très lent)"

    ```R
    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((sd(x1)^2/n1) + (sd(x2)^2/n2)))

    # Estimation des variances (écart-types au carré)
    s2.1 <- sd(x1)^2
    s2.2 <- sd(x2)^2

    # Degrés de liberté
    v <- (s2.1/n1 + s2.2/n2)^2 /(((s2.1/n1)^2/(n1 - 1)) + ((s2.2/n2)^2/(n2 - 1)))

    # Test unilatéral à gauche
    t.emp >= qt(1 - 0.05, v, lower.tail=FALSE)

    # Test unilatéral à droite
    t.emp <= qt(1 - 0.05, v, lower.tail=TRUE)

    # Test bilatéral
    t.emp >= qt(1 - 0.025, v, lower.tail=FALSE)
    t.emp <= qt(1 - 0.025, v, lower.tail=TRUE)

    # Le résultat nous dit si on peut accepter H0 ou pas
    # TRUE -> acceptation de H0
    # FALSE -> rejet de H0
    ```

## Taille d'effet

Pour mesurer la taille de l'effet, on utilise le coefficient d de Cohen:

=== "Avec R"

    ```R
    library(effectsize)

    # Coefficient d de Cohen
    d <- cohens_d(X ~ G, pooled_sd=FALSE)
    interpret_cohens_d(d)
    ```

## Exemple

On mène une expérience pour savoir si la consommation d'alcool diminue les performances au football. Pour cela, on prend deux groupes dont on compte le nombre de goals marqués en un temps donné. L'un des groupes consomme de l'alcool avant l'exercice, tandis que l'autre non.

Posons nos hypothèses:

- H~0~ : la consommation d'alcool n'a aucun effet sur les performances au football (μ~1~ = μ~2~)
- H~1~ : la consommation d'alcool diminue les performances au football (μ~1~ > μ~2~)

Nous pouvons représenter nos données graphiquement:

<figure markdown>
![Représentation graphique de nos données](test-welch-2-echantillons-independants.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

Importons nos données dans R:

=== "Avec R"

    ```R
    x1 <- c(4, 5, 3, 4, 4, 7, 6, 5, 2, 1, 9, 5, 6, 7, 1, 8, 8, 2, 1, 5, 2)
    x2 <- c(0, 2, 5, 1, 7, 3, 4, 2, 4, 2, 0, 2, 0, 2, 1, 2, 2, 3, 0, 2, 2, 3)

    n1 <- length(x1)
    n2 <- length(x2)

    X <- c(x1, x2)
    G <- factor(
        rep(1:2, c(n1, n2)),
        labels=c("Sans alcool", "Avec alcool")
    )
    ```

Vérification des conditions d'application:

=== "Avec R"

    ```R
    library(car)
    library(psych)
    
    # Test de l'homogénéité des variances
    # Avec H0: variances sont homogènes
    leveneTest(X ~ G, center=median)
    # Pr(>F) = 0.04467
    # Rejet de H0 -> les variances ne sont pas homogènes
    ```

Puisque les variances ne sont pas homogènes, nous utilisons un test de Welch **unilatéral à droite**:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à droite
    t.test(X ~ G, var.equal=FALSE, alternative="greater")
    # t = 3.5329, df = 35.569, p-value = 0.0005801
    ```

=== "Avec R (lent)"

    ```R
    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((sd(x1)^2/n1) + (sd(x2)^2/n2)))
    t.emp
    # [1] 3.532857

    # Estimation des variances (écart-types au carré)
    s2.1 <- sd(x1)^2
    s2.2 <- sd(x2)^2

    # Degrés de liberté
    v <- (s2.1/n1 + s2.2/n2)^2 /(((s2.1/n1)^2/(n1 - 1)) + ((s2.2/n2)^2/(n2 - 1)))
    v
    # [1] 35.56934

    # Test unilatéral à droite
    pt(t.emp, v, lower.tail=FALSE)
    # [1] 0.0005801461
    ```

=== "Avec R (très lent)"

    ```R
    # Valeur empirique de la variable de décision
    t.emp <- (mean(x1) - mean(x2))/sqrt(((sd(x1)^2/n1) + (sd(x2)^2/n2)))
    t.emp
    # [1] 3.532857

    # Estimation des variances (écart-types au carré)
    s2.1 <- sd(x1)^2
    s2.2 <- sd(x2)^2

    # Degrés de liberté
    v <- (s2.1/n1 + s2.2/n2)^2 /(((s2.1/n1)^2/(n1 - 1)) + ((s2.2/n2)^2/(n2 - 1)))
    v
    # [1] 35.56934

    # Test unilatéral à droite
    t.crit <- qt(1 - 0.05, v, lower.tail=TRUE)
    t.emp <= t.crit
    # [1] FALSE
    ```

Nous rejettons H~0~ et concluons que la consommation d'alcool diminue les performances au football. Maintenant, il ne reste qu'à analyser la taille de l'effet:

=== "Avec R"

    ```R
    library(effectsize)

    # Coefficient d de Cohen
    d <- cohens_d(X ~ G, pooled_sd=FALSE)
    interpret_cohens_d(d)

    # Cohen's d |       95% CI | Interpretation
    # -----------------------------------------
    # 1.08      | [0.43, 1.73] |          large
    # 
    # - Estimated using un-pooled SD.
    # - Interpretation rule: cohen1988
    ```

La taille d'effet est donc **grande**.
