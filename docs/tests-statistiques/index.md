# Tests statistiques

!!! abstract "Introduction"

    Les tests statistiques sont des outils puissants permettant d'émettre des inférences sur la base de données. Ces tests utilisent des probabilités afin de déterminer quelle hypothèse est la plus vraisemblable.

## Tableau

Afin d'arriver à la bonne conclusion, il est crucial d'utiliser le bon test. Voici donc un tableau qui permet de visualer quel test utiliser avec quel plan expérimental:

| Variable(s) | 1 échantillon <br> 1 mesure | 1 échantillon <br> 2 mesures | 2 échantillons <br> 1 mesure | k échantillons <br> 1 mesure | Mesures <br>d’association |
| --- | --- | --- | --- | --- | --- |
| **Nominale(s):** | [Test sur une proportion](test-binomial.md) <br><br> [Test d’ajustement du χ^2^ (cas simple)](test-ajustement-khi-carre.md) | Test de McNemar <br><br> Test de Bowker | [Test d’indépendance du χ^2^](test-independance-khi-carre.md) | [Test d’indépendance du χ^2^](test-independance-khi-carre.md) | [Test du χ^2^ pour deux variables catégorielles](test-independance-khi-carre.md) |
| **Ordinale(s):** | Test de Wilcoxon sur un échantillon | [Test de Wilcoxon sur mesures pairées](test-wilcoxon-mesures-pairees.md) | [Test de Mann-Whitney](test-mann-whitney.md) | [Test de Kruskal-Wallis](test-kruskal-wallis.md) | Test du coefficient de corrélation de Spearman |
| **Numérique(s):** | [Test de Student sur un échantillon](test-student-1-echantillon.md) | [Test de Student à mesures répétées](test-student-mesures-pairees.md) | [Test de l’homogénéité des variances](test-student-2-echantillons-independants.md#execution-du-test) <br><br> [Test de Student à deux groupes indépendants](test-student-2-echantillons-independants.md) <br><br> [Test de Welch](test-welch-2-echantillons-independants.md) | [Analyse de variance à un facteur de classification](anova-1-facteur-classification.md) <br><br> [Comparaisons multiples](anova-1-facteur-classification.md#comparaisons-multiples) | Test du coefficient de corrélation de Bravais-Pearson |

À présent, nous allons voir ces tests l'un après l'autre.
