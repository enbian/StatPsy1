# Test d'indépendance du khi-carré

!!! abstract "Introduction"
    Le test d'indépendance du khi-carré est un test statistique nous permettant de tester s'il existe une dépendance statistique entre les distributions de deux échantillons.

## Hypothèses

- Soit X une variable catégorielle
- Soit Y une seconde variable catégorielle

On souhaite tester s'il existe une **corrélation** entre les deux.

!!! danger "Corrélation ≠ causalité !"
    Confondre les deux constitue une erreur méthodologique extrêmement grave. La corrélation nous indique qu'il existe une **dépendance entre certains facteurs**, mais, contrairement à la causalité, elle ne nous **permet pas d'indiquer la direction** de cette dernière.

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| X et Y sont indépendantes | X et Y ne sont pas indépendantes |

## Tableau des effectifs

Nous pouvons construire un tableau des effectifs observés avec nos échantillons:

|  | Y | |  |  |
| :--- | :---: | :---: | :---: | ---: |
| **X** | **y~1~** | **y~2~** | **y~3~** | **Σ** |
| **x~1~** | n~11~ | n~12~ | n~13~ | n~1~. |
| **x~2~** | n~21~ | n~22~ | n~23~ | n~2~. |
| **x~3~** | n~31~ | n~32~ | n~33~ | n~3~. |
| **Σ** | n.~1~ | n.~2~ | n.~3~ | n.. |

Puis le tableau des effectifs théoriques, avec e~ij~ = (n~i~. * n.~j~) / n..:

|  | Y | |  |  |
| :--- | :---: | :---: | :---: | ---: |
| **X** | **y~1~** | **y~2~** | **y~3~** | **Σ** |
| **x~1~** | e~11~ | e~12~ | e~13~ | n~1~. |
| **x~2~** | e~21~ | e~22~ | e~23~ | n~2~. |
| **x~3~** | e~31~ | e~32~ | e~33~ | n~3~. |
| **Σ** | n.~1~ | n.~2~ | n.~3~ | n.. |

## Exécution du test

- χ^2^~emp~ : la valeur empirique de la variable de décision = Σ~i~Σ~j~ (n~ij~ - e~ij~)^2^ / e~ij~ = (n~11~ - e~11~)^2^ / e~11~ + (n~12~ - e~12~)^2^ / e~12~ + ...

=== "Avec R"

    ```R
    res <- chisq.test(TAB, correct=FALSE)

    res$observed  # Tableau des effectifs observés
    res$expected  # Tableau des effectifs théoriques
    res$observed - res$expected  # Tableau des résidus
    res$residuals  # Tableau des résidus standardisés
    
    khi2.emp <- res$statistic  # Valeur empirique de la variable de décision
    res$p.value  # Probabilité critique
    ```

## Taille d'effet

Pour calculer la taille de l'effet, on utilise le coefficient V de Cramer. Plus ce dernier est proche de 0, plus il s'agit d'une situation dans laquelle les variables sont indépendantes. Au contraire, plus V est proche de 1, plus nous sommes en situation de dépendance fonctionnelle.

- n : la somme des effectifs observés
- χ^2^~min~ : valeur minimale du χ^2^~emp~ = 0
- χ^2^~max~ : valeur maximale du χ^2^~emp~ = n * [min(I, J) − 1]
- df*: les degrés de liberté du coefficient V de Cramer = min(I, J) − 1

=== "Avec R (rapide)"

    ```R
    library(rcompanion)
    cramerV(TAB)  # Coefficient V de Cramer
    ```

=== "Avec R (lent)"

    ```R
    n <- sum(TAB)  # Somme des effectifs
    df <- min(dim(TAB) - 1)  # Degrés de liberté du coefficient V de Cramer
    V <- sqrt(khi2.emp/(n*df))  # Coefficient V de Cramer
    ```

Puis, nous pouvons utiliser la règle d'interprétation suivante:

| df* | petite | moyenne | grande |
| :--- | :---: | :---: | :---: |
| 1 | 0.10 | 0.30 | 0.50 |
| 2 | 0.07 | 0.21 | 0.35 |
| 3 | 0.06 | 0.17 | 0.29 |
| 4 | 0.05 | 0.15 | 0.25 |
| 5 | 0.04 | 0.13 | 0.22 |

## Analyse des résidus

L'analyse des résidus nous permet de savoir entre quelles modalités de chaque variable il y a un effet significatif et si cet effet est positif ou négatif.
Pour cela, on analyse le tableau des résidus standardisés obtenu précédemment. Toutes les cellules dont la valeur est supérieure à 2 (ou même à 1 selon la situation) méritent une interprétation.

Voici notre tableau des résidus standardisés, avec RSij = (n~ij~ − e~ij~)/sqrt(e~ij~):

|  | Y | |  |
| :--- | :---: | :---: | :---: |
| **X** | **y~1~** | **y~2~** | **y~3~** |
| **x~1~** | RS~11~ | RS~12~ | RS~13~ |
| **x~2~** | RS~21~ | RS~22~ | RS~23~ |
| **x~3~** | RS~31~ | RS~32~ | RS~33~ |

## Exemple

On cherche à savoir si le sommeil a un impact sur les résultats scolaires. Pour ce faire, on compare donc les résultats de 100 élèves à un test en notant leur nombre d'heures de sommeil la nuit précédente.

Ici, nos variables sont les suivantes:

- X: Heures de sommeil (variable catégorielle à 3 modalités: `1-4h`, `5-7h`, `8-10h`)
- Y: Résultat à l'épreuve (variable catégorielle à 4 modalités: `excellent`, `bon`, `moyen`, `mauvais`)

Nous pouvons poser nos hypothèses:

- H~0~: les performances scolaires ne sont pas associées au nombre d'heures de sommeil
- H~1~: les performances scolaires sont associées au nombre d'heures de sommeil

|  | Résultats | |  |  | |
| :--- | :---: | :---: | :---: | :---: | ---: |
| **Heures de sommeil**  | **Excellent** | **Bon** | **Moyen** | **Mauvais** | **Σ** |
| **1-4h**  | 0  | 2 | 9 | 12 | 23 |
| **5-7h**  | 9  | 9  | 8  |  6 | 32 |
| **8-10h** | 18  | 9  | 10 | 8  | 45 |
| **Σ**     | 27 | 20 | 27 | 26 | 100 |

Nous pouvons dès lors représenter nos distributions graphiquement:

<figure markdown>
![Distribution des effectifs observés](test-independance-khi-carre.png)
<ficaption>Distribution des effectifs observés</ficaption>
</figure>

Puis, nous effectuons le test:

=== "Avec R"

    ```R
    TAB <- matrix(
        c(0, 2, 9, 12, 9, 9, 8, 6, 18, 9, 10, 8),
        byrow = TRUE,
        ncol = 4,
        dimnames = list(
            SOMMEIL = c("1-4h", "5-7h", "8-10h"),
            RESULTATS = c("Excellent", "Bon", "Moyen", "Mauvais")
        )
    )

    # Test d'indépendance du khi-carré
    res <- chisq.test(TAB, correct=FALSE)
    round(res$p.value, 3)
    # [1] 0.002
    ```

Ici, nous pouvons voir que la probabilité critique p = 0.002. Puisque `p=0.002 < α=0.05`, nous pouvons rejetter H~0~. Nous concluons qu'il semble bel et bien y avoir une corrélation entre heures de sommeil et performances scolaires.

!!! note
    Dans ce cas précis, nous pouvons connaître la direction de cette corrélation, puisqu'il est impossible que ce soient les résultats de ce test précis qui aient eu un impact rétroactif sur le nombre d'heures de sommeil.

Ensuite, nous devons interpréter la taille d'effet:

=== "Avec R (rapide)"

    ```R
    library(rcompanion)

    df <- min(dim(TAB) - 1)  # Degrés de liberté du coefficient V de Cramer
    df
    # 2

    cramerV(TAB)  # Coefficient V de Cramer
    # 0.325
    ```

=== "Avec R (lent)"

    ```R
    n <- sum(TAB)  # Somme des effectifs

    df <- min(dim(TAB) - 1)  # Degrés de liberté du coefficient V de Cramer
    df
    # 2

    V <- sqrt(khi2.emp/(n*df))  # Coefficient V de Cramer
    round(V, 3)
    # 0.325
    ```

Puisque V = 0.325 et que df\* = 2, en nous référent à la règle d'interprétation, nous pouvons conclure que la taille d'effet est **moyenne**.

Nous souhaitons maintenant savoir dans quelles cellule cet effet est significatif. Pour cela, nous construisons la tableau des résidus standardisés:

|  | Résultats | | |  |
| :--- | :---: | :---: | :---: | :---: |
| **Heures de sommeil**  | **Excellent** | **Bon** | **Moyen** | **Mauvais** |
| **1-4h**  | -2.49 * | -1.21 * | 1.12 * | 2.46 * |
| **5-7h**  | 0.12 | 1.03 * | -0.22 | -0.80 |
| **8-10h** | 1.68 * | 0.00 | -0.62 | -1.08 * |

!!! info
    Les cellules dans lesquels un effet significatif existe sont marquées d'une astérisque (\*).

Nous pouvons également représenter ces effets par un diagramme d'association:

<figure markdown>
![Diagramme d'association](test-independance-khi-carre-2.png)
<ficaption>Diagramme d'association</ficaption>
</figure>

Nous pouvons donc conclure en soulignant qu'un faible nombre d'heures de sommeil est associé à une faible performance scolaire, alors qu'un haut nombre d'heures de sommeil semble être associé à une meilleure performance scolaire.
