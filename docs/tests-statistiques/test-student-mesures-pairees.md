# Test de Student sur mesures pairées

!!! abstract "Introduction"
    Le t-test de Student sur mesures pairées (ou répétées) est un test paramétrique nous permettant de déterminer s'il existe une différence significative entre 
    deux mesures des scores d'un même échantillon. Le plus souvent, ce test est utilisé pour mesurer l'effet d'une variable sur le long terme.

## Variables

- D : les différences entre les scores des deux mesures = X~1~ - X~2~
- d̅ : moyenne des différences entre les deux mesures
- s~d~ : l'écart-type des différence entre les deux mesures
- t~emp~ : la valeur empirique de la variable de décision = [(d̅ - 0) / s~d~] * sqrt(n)

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Nos deux mesures semblent provenir d'une même population. | Nos deux mesures semblent provenir de populations différentes. |
| μ~1~ = μ~2~ | μ~1~ ≠ μ~2~  |
| μ~D~ = 0 | μ~D~ ≠ 0 |

## Exécution du test

!!! success "Conditions d'application"

    Pour que le test puisse être utilisé:
    
    - Si `n <= 20` : les différences doivent **se distribuer normalement**
    - Si `n > 20` : il ne doit **pas y avoir de différences extrêmes**

    Si ces conditions ne sont pas remplies, nous devons utiliser un test non-paramétrique à la place (moins puissant). Dans ce cas, il s'agit d'un [test de Wilcoxon sur mesures pairées](test-wilcoxon-mesures-pairees.md).

Vérification des conditions d'application:

=== "Avec R"

    ```R
    D <- T1 - T2  # différences entre les scores
    n <- length(D)  # effectifs

    # Si n <= 20:
    # Test de normalité de Shapiro-Wilk
    shapiro.test(D)
    # Si la p-value de ce test est inférieure à 0.05 (seuil alpha),
    # cela signifie que la distribution des différences n'est pas normale

    # Si n > 20:
    # Mise en évidence des différences extrêmes
    outlier <- abs(D - median(D)) > 3*mad(D, constant = 1)
    D[outlier]
    # Si le résultat est 0 => pas de différences extrêmes

    ```

Exécution du test avec R:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche (H1: μ1 < μ2)
    t.test(T1, T2, paired=TRUE, alternative="less")

    # Test unilatéral à droite (H1: μ1 > μ2)
    t.test(T1, T2, paired=TRUE, alternative="greater")

    # Test bilatéral (H1: μ1 ≠ μ2)
    t.test(T1, T2, paired=TRUE, alternative="two.sided")
    ```

=== "Avec R (lent)"

    ```R
    d.bar <- mean(D)  # moyenne
    s.d <- sd(D)  # écart-type
    t.emp <- (d.bar)/(s.d/sqrt(n))  # valeur empirique de la variable de décision

    # Test unilatéral à gauche
    pt(t.emp, n-1, lower.tail=TRUE)

    # Test unilatéral à droite
    pt(t.emp, n-1, lower.tail=FALSE)

    # Test bilatéral
    2*min(
        pt(t.emp, n-1, lower.tail=FALSE), 
        pt(t.emp, n-1, lower.tail=TRUE)
    )
    ```

=== "Avec R (très lent)"

    ```R
    d.bar <- mean(D)  # moyenne
    s.d <- sd(D)  # écart-type
    t.emp <- (d.bar)/(s.d/sqrt(n))  # valeur empirique de la variable de décision

    # On détermine la/les valeur(s) critique(s)
    # et on regarde si t empirique est dans la 
    # ou les zone(s) critique(s)

    # Test unilatéral à gauche
    t.emp >= qt(1 - 0.05, n-1, lower.tail=FALSE)

    # Test unilatéral à droite
    t.emp <= qt(1 - 0.05, n-1, lower.tail=TRUE)

    # Test bilatéral
    t.emp >= qt(1 - 0.025, n-1, lower.tail=FALSE)
    t.emp <= qt(1 - 0.025, n-1, lower.tail=TRUE)
    ```

## Exemple

Nous cherchons l'effet d'un médicament pour améliorer le sommeil. Nous mesurons le temps nécessaire pour s'endormir chez les mêmes individus, une fois avant le traitement, et une fois après le traitement. Nous voulons de cette manière tester si le traitement a un effet sur le sommeil. Ici, nous allons utiliser **un seuil de 1%**.

!!! info "Pourquoi 1%?"
    Alors qu'en psychologie, on utilise par convention le seuil de 5%, on utilise généralement un seuil de 1% en pharmacologie. Cela s'explique par le fait qu'une erreur est généralement bien plus dangereuse et/ou coûteuse dans cette discipline.

    Néanmoins, en fonction de l'impact que peut avoir une étude, un seuil critique plus précis peut être adopté, même en psychologie.

!!! danger "Attention: mauvaise méthodologie"
    Ceci est un exemple purement fictif. Dans la réalité, une expérience comme celle-ci nécessiterait évidemment un **groupe contrôle** à qui l'on donnerait un placebo (ou un autre traitement dans le but de déterminer si le nouveau traitement est plus efficace que celui qui existe déjà).

    Autrement, il n'est pas possible d'affirmer avec certitude que les différences - s'il y en a - sont dûes à une substance active et non au simple fait de recevoir un traitement.

Voici les scores des deux mesures:

| Avant le traitement (T~1~) | Après le traitement (T~2~) | D (T~1~ - T~2~) |
| :--- | :--- | ---: |
| 25 | 24 | 1 |
| 15 | 12 | 3 |
| 17 | 18 | -1 |
| 20 | 17 | 3 |
| 15 | 9 | 6 |
| 17 | 14 | 3 |
| 18 | 14 | 4 |
| 20 | 19 | 1 |
| 16 | 19 | -3 |
| 23 | 15 | 8 |
| 18 | 18 | 0 |
| 22 | 20 | 2 |

Nous pouvons maintenant importer ces données dans R et vérifier les conditions d'application:

=== "Avec R"

    ```R
    T1 <- c(25, 15, 17, 20, 15, 17, 18, 20, 16, 23, 18, 22)
    T2 <- c(24, 12, 18, 17, 9, 14, 14, 19, 19, 15, 18, 20)

    D <- T1 - T2  # différences entre les scores
    n <- length(D)  # effectifs

    # n = 12, vérifions si la distribution des différences est normale:
    shapiro.test(D)
    # W = 0.98168, p-value = 0.9895
    ```

Puisque la distribution est normale, nous pouvons réaliser notre t-test **unilatéral à droite**, puisque nous cherchons à voir si μ~1~ > μ~2~, ce qui signifierait que le traitement a un effet:

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à droite (H1: μ1 > μ2)
    t.test(T1, T2, paired=TRUE, alternative="greater")
    # t = 2.608, df = 11, p-value = 0.01217
    ```

=== "Avec R (lent)"

    ```R
    d.bar <- mean(D)  # moyenne
    s.d <- sd(D)  # écart-type
    t.emp <- (d.bar)/(s.d/sqrt(n))  # valeur empirique de la variable de décision

    # Test unilatéral à droite (H1: μ1 > μ2)
    pt(t.emp, n-1, lower.tail=FALSE)
    # [1] 0.01217267
    ```

=== "Avec R (très lent)"

    ```R
    d.bar <- mean(D)  # moyenne
    s.d <- sd(D)  # écart-type
    t.emp <- (d.bar)/(s.d/sqrt(n))  # valeur empirique de la variable de décision
    t.crit <- qt(1 - 0.01, n-1, lower.tail=FALSE)  # valeur critique

    # Test unilatéral à droite (H1: μ1 > μ2)
    t.emp
    # [1] 2.607974

    t.crit
    # [1] 2.718079
    
    t.emp <= t.crit
    # [1] TRUE
    ```

Puisque `p = 0.01217 > α=0.01`, nous ne pouvons pas rejetter H0. Nous concluons donc que le traitement n'a pas d'effet significatif. Néanmoins, si nous avions choisi un seuil de significativité de 5%, nous aurions détecté un effet.
