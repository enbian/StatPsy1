# Test de Wilcoxon sur mesures pairées

!!! abstract "Introduction"

    Le test de Wilcoxon sur mesures pairées est l'équivalent non-paramétrique du [test de Student sur mesures pairées](test-student-mesures-pairees.md).

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Nos deux mesures semblent provenir d'une même population. | Nos deux mesures semblent provenir de populations différentes. |
| μ~1~ = μ~2~ | μ~1~ ≠ μ~2~  |
| μ~D~ = 0 | μ~D~ ≠ 0 |

## Exécution du test

Le test de Wilcoxon est un test non-paramétrique. Cela signifie que l'on considère que les données sont ordinales. On attribue donc des rangs à la valeur absolue de leurs différences.

Par exemple, si nos différences sont les suivantes, nous attribuons les rangs (= numérotons les différences) de cette manière:

| D | \|D\| | Rang | Explication |
| --- | --- | --- | --- |
| -4 | 4 | **3** | ici, \|D\| est la **3^e^** plus petite |
| 6 | 6 | **4** | ici, \|D\| est la **4^e^** plus petite |
| 1 | 1 | **1** | ici, \|D\| est la **1^e^** plus petite |
| -3 | 3 | **2** | ici, \|D\| est la **2^e^** plus petite |

On peut poser les variables suivantes:

- T^+^ : la valeur empirique de la variable de décision = la somme de tous les rangs dont la différence est positive
- T^-^ = la somme de tous les rangs dont la différence est négative

=== "Avec R (rapide)"

    ```R
    # Test unilatéral à gauche (H1: μ1 < μ2)
    wilcox.test(D, mu=0, alternative="less")
    wilcox.test(X1, X2, paired=TRUE, alternative="less")

    # Test unilatéral à droite (H1: μ1 > μ2)
    wilcox.test(D, mu=0, alternative="greater")
    wilcox.test(X1, X2, paired=TRUE, alternative="greater")

    # Test bilatéral (H1: μ1 ≠ μ2)
    wilcox.test(D, mu=0, alternative="two.sided")
    wilcox.test(X1, X2, paired=TRUE, alternative="two.sided")
    ```

=== "Avec R (lent)"

    !!! warning "Attention"

        La méthode lente n'est à utiliser que s'il n'y a pas d'ex-aequo dans les rangs (que deux rangs n'ont pas la même valeur) et s'il n'y a pas de différence nulle (0). Pour être plus sûr·e·x, il est conseillé de toujours utiliser la méthode rapide (qui s'occupe de faire les corrections nécessaires au cas où), sauf indication contraire.

    ```R
    D <- X1 - X2  # différences
    R <- rank(abs(D))  # attribution des rangs
    n = length(D)  # effectifs

    t.plus <- sum(R[D > 0])
    t.moins <- sum(R[D < 0])

    # Test unilatéral à gauche
    psignrank(q=t.plus, n=n, lower.tail=TRUE)
    sum(dsignrank(0:t.plus, n=n))

    # Test unilatéral à droite
    psignrank(q=t.moins, n=n, lower.tail=TRUE)
    sum(dsignrank(0:t.moins, n=n))

    # Test bilatéral
    2*min(
        psignrank(q=t.plus, n=n, lower.tail=TRUE),
        psignrank(q=t.moins, n=n, lower.tail=TRUE)
    )
    2*min(
        sum(dsignrank(0:t.plus, n=n)),
        sum(dsignrank(0:t.moins, n=n))
    )
    ```

## Exemple

Nous souhaitons savoir si la mémoire de travail est meilleure le matin que le soir. On effectue donc des mesures chez des individus, une fois le matin, et une fois le soir. On mesure le nombre de mots retenus dans un exercice de mémoire.

Nous pouvons représenter nos données graphiquement:

<figure markdown>
![Représentation graphique de nos données](test-wilcoxon-mesures-pairees.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

=== "Avec R"

    ```R
    Matin <- c(7,  4,  8, 11,  2,  1,  4,  5, 10, 19, 3, 5)
    Soir <- c(8, 10, 10,  4, 11, 12, 12, 10, 20,  1, 6, 9)

    D <- Matin - Soir
    ```

Vérification des conditions d'application:

=== "Avec R"

    ```R
    n <- length(D)
    n
    # [1] 12

    shapiro.test(D)
    # W = 0.82931, p-value = 0.02058
    # Les différences ne se distribuent pas normalement
    ```

Attribution des rangs:

| Matin | Soir | D | \|D\| | Rang |
| --- | --- | --- | --- | --- |
| 7 | 8 | -1 | 1 | 1 |
| 4 | 10 | -6  | 6 | 6 |
| 8 | 10 | -2 | 2 | 2 |
| 11 | 4 | 7 | 7 | 7 |
| 2 | 11 | -9 | 9 | 9 |
| 1 | 12 | -11 | 11 | 11 |
| 4 | 12 | -8 | 8 | 8 |
| 5 | 10 | -5 | 5 | 5 |
| 10 | 20 | -10 | 10 | 10 |
| 19 | 1 | 18 | 18 | 12 |
| 3 | 6 | -3 | 3 | 3 |
| 5 | 9 | -4 | 4 | 4 |

Nous pouvons représenter la distribution de T^+^ sous H~0~ graphiquement:

<figure markdown>
![Distribution de T^+^ sous H~0~ avec n=12](test-wilcoxon-mesures-pairees-2.png)
<ficaption>Distribution de T^+^ sous H~0~ avec n=12</ficaption>
</figure>

Exécution du test:

=== "Avec R (rapide)"

    ```R
    wilcox.test(D, mu=0, alternative="greater")
    # V = 19, p-value = 0.9451
    ```

=== "Avec R (lent)"

    ```R
    D <- Matin - Soir  # différences
    R <- rank(abs(D))  # attribution des rangs
    n = length(D)  # effectifs

    # valeur empirique de la variable de décision
    t.plus <- sum(R[D > 0])
    t.moins <- sum(R[D < 0])
    c(t.plus, t.moins)
    # [1] 19 59

    # Test unilatéral à droite
    psignrank(q=t.moins, n=n, lower.tail=TRUE)
    # [1] 0.9450684
    sum(dsignrank(0:t.moins, n=n))
    # [1] 0.9450684
    ```

Puisque p > 0.05, nous ne pouvons pas rejetter H~0~. Par conséquent, nous ne pouvons pas affirmer la faculté de mémorisation est meilleure le matin qu'elle ne l'est le soir.
