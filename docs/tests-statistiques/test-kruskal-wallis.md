# Test de Kruskal-Wallis

!!! abstract "Introduction"

    Le test de Kruskal-Wallis est l'équivalent non-paramétrique de [l'analyse de variance (ANOVA) à un facteur de classification](anova-1-facteur-classification.md).

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Tous nos échantillons semblent provenir d'une même population. | Tous nos échantillons ne semblent pas provenir d'une seule et même population. |
| μ~1~ = μ~2~ = ... = μ~k~ | μ~1~ ≠ μ~2~ \|\| ... \|\| μ~1~ ≠ μ~k~ \|\| μ~2~ ≠ μ~k~ |

## Exécution du test

Sous H~0~, la variable de décision suit une loi du khi-carré à m-1 degrés de liberté.

<figure markdown>
![Distribution du khi-carré à 2 degrés de liberté](test-kruskal-wallis.png)
<ficaption>Distribution du khi-carré à 2 degrés de liberté</ficaption>
</figure>

=== "Avec R (rapide)"

    ```R
    # Test de Kruskal-Wallis
    kruskal.test(X ~ G)
    ```

=== "Avec R (lent)"

    !!! warning "Attention"

        La méthode lente n'est à utiliser que s'il n'y a pas d'ex-aequo dans les rangs (que deux rangs n'ont pas la même valeur) Pour être plus sûr·e·x, il est conseillé de toujours utiliser la méthode rapide (qui s'occupe de faire les corrections nécessaires au cas où), sauf indication contraire.

    ```R
    # attribution des rangs
    R <- rank(abs(X))

    T1 <- sum(R[is.element(X, x1)])  # rangs issus de x1
    T2 <- sum(R[is.element(X, x2)])  # rangs issus de x2
    T3 <- sum(R[is.element(X, x3)])  # rangs issus de x3

    n <- n1 + n2 + n3  # somme des effectifs

    # valeur empirique de la variable de décision
    H <- 12/(n*(n+1))*((T1^2/n1)+(T2^2/n2)+(T3^2/n3))-3*(n+1)

    pchisq(H, df=m-1, lower.tail=FALSE)  # probabilité critique
    ```

## Comparaisons multiples

Pour connaître quelles sont les différences qui sont significatives, nous devons utiliser des comparaisons multiples (non-paramétriques):

=== "Avec R"

    ```R
    pairwise.wilcox.test(x=X, g=G, p.adjust.method="bonferroni")
    ```

!!! note

    La méthode d'ajustement de `holm` est la plus précise et donc celle qui est recommandée. Cependant, il est aussi possible de ne pas effectuer de correction et d'utiliser `none` à la place, ou alors d'utiliser la méthode de `bonferroni`.

## Exemple

Vu que je n'avais plus d'idée d'exemple, j'en ai repris un.
Nous souhaitons tester s'il y a un lien entre classe sociale et nombre d'années d'études.


Importons nos données dans R:

=== "Avec R"

    ```R
    x1 <- c(11, 10, 12, 11, 11, 11, 9, 2)
    x2 <- c(15, 14, 7, 15, 14, 11)
    x3 <- c(16, 20, 25, 18, 19, 22, 18, 25, 6)

    n1 <- length(x1)
    n2 <- length(x2)
    n3 <- length(x3)

    X <- c(x1, x2, x3)
    G <- factor(
        rep(1:3, c(n1, n2, n3)),
        labels=c(
            "Précaire", "Moyenne", "Supérieure"
        )
    )
    ```


Vérification des conditions d'application:

=== "Avec R"

    ```R
    # m = 3 < 9

    # n1 = 8, n2 = 6, n3 = 9
    # nj < 15

    # Données sont-elles issues de distributions normales?
    shapiro.test(residuals(lm(X ~ G)))
    # W = 0.85572, p-value = 0.00346
    # acceptation de H0 -> distributions ne sont pas normales
    ```

Puisque les conditions d'application d'une ANOVA ne sont pas remplies, nous devons effectuer un test de Kruskal-Wallis:

=== "Avec R (rapide)"

    ```R
    kruskal.test(X ~ G)
    # Kruskal-Wallis chi-squared = 11.376, df = 2, p-value = 0.003387
    ```

=== "Avec R (lent)"

    !!! failure "Attention: ne pas utiliser la méthode lente !"
        Ici, certains rangs ont des ex-aequos (par exemple il y a plusieurs fois la valeur `11` dans nos données). Nous ne pouvons donc pas utiliser la méthode lente.

Puisque p < 0.05, nous rejettons H~0~ et concluons qu'il y a une différence significative entre nos échantillons. Il ne reste qu'à déterminer où se situent ces différences:

=== "Avec R"

    ```R
    pairwise.wilcox.test(x=X, g=G, p.adjust.method="holm")
    ```

|  | Précaire | Moyenne |
| --- | --- | --- |
| **Moyenne** | 0.074 | - |
| **Supérieure** | 0.017 | 0.031 |

Nous constatons, qu'il y a, au seuil de 5%, une différence significative entre toutes les classes sociales, sauf entre la classe la plus précaire et la classe moyenne.
