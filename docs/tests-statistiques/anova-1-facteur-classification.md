# Analyse de variance de Fisher

!!! abstract "Introduction"

    L'analyse de variance (ANOVA) de Fisher est un test paramétrique permettant de comparer _k_ échantillons et de démontrer s'ils sont tous issus d'une même population ou de populations différentes.

## Hypothèses

| Hypothèse nulle (H~0~) | Hypothèse alternative (H~1~) |
| --- | --- |
| Tous nos échantillons semblent provenir d'une même population. | Tous nos échantillons ne semblent pas provenir d'une seule et même population. |
| μ~1~ = μ~2~ = ... = μ~k~ | μ~1~ ≠ μ~2~ \|\| ... \|\| μ~1~ ≠ μ~k~ \|\| μ~2~ ≠ μ~k~ |

## Exécution du test

!!! success "Conditions d'application"

    Pour pouvoir utiliser une ANOVA de Fisher:

    - Si m > 9:
        - Si n~j~ > 20 ou alors si les données sont issues de distributions normales:
            - Si les variances sont homogènes, utiliser l'ANOVA de Fisher
            - Sinon utiliser [l'ANOVA de Welch](anova-welch-1-facteur-classification.md)
        - Sinon, utiliser [un test de Kruskal-Wallis](test-kruskal-wallis.md)
    - Sinon:
        - Si n~j~ > 15 ou alors si les données sont issues de distributions normales:
            - Si les variances sont homogènes, utiliser l'ANOVA de Fisher
            - Sinon utiliser [l'ANOVA de Welch](anova-welch-1-facteur-classification.md)
        - Sinon, utiliser [un test de Kruskal-Wallis](test-kruskal-wallis.md)

L'ANOVA suit une distribution de Fisher à m − 1 et n − m degrés de liberté:

<figure markdown>
![Distribution de Fisher à 2 et 40 degrés de liberté](anova-1-facteur-classification.png)
<ficaption>Distribution de Fisher à 2 et 40 degrés de liberté</ficaption>
</figure>

=== "Avec R (rapide)"

    ```R
    # ANOVA (Fisher)
    summary(aov(X ~ G))
    ```

=== "Avec R (lent)"

    ```R
    nj <- table(G)  # tableau des effectifs
    mj <- by(X, G, mean)  # moyennes
    sj <- by(X, G, sd)  # écart-types

    m <- length(nj)  # nombre d'échantillons
    n <- sum(nj)  # total des effectifs
    M <- sum(nj*mj)/n  # moyenne pondérée

    SCA <- sum(nj*(mj - M)^2)  # somme des carrés associés au facteur
    SCR <- sum((nj - 1)*sj^2)  # somme des carrés résiduels

    CMA <- SCA/(m - 1)  # carré moyen associé au facteur
    CMR <- SCR/(n - m)  # carré moyen résiduel

    F.emp <- CMA/CMR  # valeur empirique de la variable de décision
    p <- pf(F.emp, m-1, n-m, lower.tail = FALSE)  # probabilité critique
    ```

=== "Avec R (très lent)"

    ```R
    nj <- table(G)  # tableau des effectifs
    mj <- by(X, G, mean)  # moyennes
    sj <- by(X, G, sd)  # écart-types

    m <- length(nj)  # nombre d'échantillons
    n <- sum(nj)  # total des effectifs
    M <- sum(nj*mj)/n  # moyenne pondérée

    SCA <- sum(nj*(mj - M)^2)  # somme des carrés associés au facteur
    SCR <- sum((nj - 1)*sj^2)  # somme des carrés résiduels

    CMA <- SCA/(m - 1)  # carré moyen associé au facteur
    CMR <- SCR/(n - m)  # carré moyen résiduel

    F.emp <- CMA/CMR  # valeur empirique de la variable de décision
    F.crit <- qf(1 - 0.05, m-1, n-m, lower.tail=TRUE)  # valeur critique

    F.emp <= F.crit
    ```

## Comparaisons multiples

Après avoir effectué l'ANOVA, il est possible de savoir s'il y a un effet. Néanmoins, il n'est pas possible de savoir entre quels échantillons ces différences sont significatives. Pour ce faire, nous devons utiliser les comparaisons multiples:

=== "Avec R"

    ```R
    # Comparaisons multiples
    pairwise.t.test(x=X, g=G, p.adjust.method="holm", pool.sd=TRUE)
    ```

!!! note

    La méthode d'ajustement de `holm` est la plus précise et donc celle qui est recommandée. Cependant, il est aussi possible de ne pas effectuer de correction et d'utiliser `none` à la place, ou alors d'utiliser la méthode de `bonferroni`.

## Exemple

Nous voulons tester l'efficacité de plusieurs vaccins contre une maladie. Pour ce faire, nous avons trois groupe: un groupe contrôle, qui reçoit un placebo, un groupe qui reçoit un vaccin, ainsi qu'un dernier groupe qui reçoit un autre vaccin. Nous mesurons le taux d'anticorps présent dans le corps une semaine après la vaccination.

Ici, nous allons utiliser **un seuil de 0.1%**, car une erreur de première espèce pourrait avoir des conséquences désastrueuses sur des vies humaines.

Nos hypothèses sont les suivantes:

- H~0~ : toutes les conditions sont équivalentes (les vaccins ne fonctionnent pas)
- H~1~ : toutes les conditions ne sont pas équivalentes (en tout cas un vaccin fonctionne)

Nous pouvons représenter nos données graphiquement:

<figure markdown>
![Représentation graphique de nos données](anova-1-facteur-classification-2.png)
<ficaption>Représentation graphique de nos données</ficaption>
</figure>

Importons nos données dans R:


=== "Avec R"

    ```R
    x1 <- c(
        0.21, 0.23, 0.17, 0.23, 0.12, 
        0.14, 0.21, 0.15, 0.16, 0.17, 
        0.18, 0.20, 0.16, 0.15, 0.16, 
        0.18, 0.22, 0.14, 0.21, 0.18,
        0.23, 0.22, 0.19, 0.20, 0.22
    )
    x2 <- c(
        0.31, 0.44, 0.42, 0.43, 0.50, 
        0.29, 0.34, 0.47, 0.36, 0.37, 
        0.41, 0.43, 0.34, 0.38, 0.39, 
        0.31, 0.32, 0.41, 0.37, 0.35,
        0.38, 0.39, 0.40, 0.36, 0.41
    )
    x3 <- c(
        0.32, 0.46, 0.39, 0.35, 0.34, 
        0.43, 0.44, 0.41, 0.43, 0.45, 
        0.39, 0.43, 0.44, 0.39, 0.39, 
        0.37, 0.43, 0.47, 0.42, 0.42,
        0.44, 0.44, 0.43, 0.39, 0.50
    )
    n1 <- length(x1)
    n2 <- length(x2)
    n3 <- length(x3) 

    X <- c(x1, x2, x3)
    G <- factor(
        rep(1:3, c(n1, n2, n3)),
        labels=c(
            "Placebo", "Vaccin 1", "Vaccin 2"
        )
    )
    ```

Vérifions maintenant les conditions d'application:

=== "Avec R"

    ```R
    library(car)
    library(psych)

    # On a 3 échantillons, donc m < 9

    # Statistique descriptive
    by(X, G, describe, skew=FALSE, range=FALSE)

    # Tous les effectifs > 15

    # Test de l'homogénéité des variances
    # Avec H0: variances sont homogènes
    leveneTest(X ~ G, center=median)
    # Pr(>F) = 0.2519  -> acceptation de H0
    # Les variances sont homogènes
    ```

Puisque les conditions d'applications sont remplies, nous pouvons utiliser une ANOVA selon Fisher.

Voici une représentation graphique de notre distribution de Fisher à m - 1 et n - m degrés de liberté:

<figure markdown>
![Distribution de Fisher à 2 et 72 degrés de liberté](anova-1-facteur-classification-3.png)
<ficaption>Distribution de Fisher à 2 et 72 degrés de liberté</ficaption>
</figure>

=== "Avec R (rapide)"

    ```R
    summary(aov(X ~ G))
    #             Df Sum Sq Mean Sq F value Pr(>F)    
    # G            2 0.7743  0.3872   216.4 <2e-16 ***
    # Residuals   72 0.1288  0.0018                   
    # ---
    # Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    ```

=== "Avec R (lent)"

    ```R
    nj <- table(G)  # tableau des effectifs
    mj <- by(X, G, mean)  # moyennes
    sj <- by(X, G, sd)  # écart-types

    m <- length(nj)  # nombre d'échantillons
    n <- sum(nj)  # total des effectifs
    M <- sum(nj*mj)/n  # moyenne pondérée

    SCA <- sum(nj*(mj - M)^2)  # somme des carrés associés au facteur
    SCR <- sum((nj - 1)*sj^2)  # somme des carrés résiduels
    CMA <- SCA/(m - 1)  # carré moyen associé au facteur
    CMR <- SCR/(n - m)  # carré moyen résiduel

    F.emp <- CMA/CMR  # valeur empirique de la variable de décision

    F.emp
    # [1] 216.439

    p <- pf(F.emp, m-1, n-m, lower.tail=FALSE)  # probabilité critique

    p
    # [1] 3.542058e-31
    ```

=== "Avec R (très lent)"

    ```R
    nj <- table(G)  # tableau des effectifs
    mj <- by(X, G, mean)  # moyennes
    sj <- by(X, G, sd)  # écart-types

    m <- length(nj)  # nombre d'échantillons
    n <- sum(nj)  # total des effectifs
    M <- sum(nj*mj)/n  # moyenne pondérée

    SCA <- sum(nj*(mj - M)^2)  # somme des carrés associés au facteur
    SCR <- sum((nj - 1)*sj^2)  # somme des carrés résiduels
    CMA <- SCA/(m - 1)  # carré moyen associé au facteur
    CMR <- SCR/(n - m)  # carré moyen résiduel

    F.emp <- CMA/CMR  # valeur empirique de la variable de décision
    F.crit <- qf(1 - 0.001, m-1, n-m, lower.tail=TRUE)  # valeur critique

    F.emp
    # [1] 216.439
    F.crit
    # [1] 7.614996

    F.emp <= F.crit
    # [1] FALSE
    ```

Au seuil de 0.1%, nous concluons qu'il y a des différences entre les groupes. Néanmoins, nous devons encore savoir où se trouvent ces différences:

=== "Avec R"

    ```R
    pairwise.t.test(x=X, g=G, p.adjust.method="holm", pool.sd=TRUE)
    ```

Voici les résultats de nos comparaisons multiples:

|  | Placebo | Vaccin 1 |
| --- | --- | --- |
| **Vaccin 1** | <2e-16 | - |
| **Vaccin 2** | <2e-16 | 0.01 |

Néanmoins, cette information ne nous permet pas de savoir la direction de cette différence - c'est-à-dire si le placebo est plus ou moins efficace que les vaccins (même si dans cette situation cela semble évident). Nous devons donc réaliser des t-tests pour vérifier:

=== "Avec R"

    ```R
    t.test(x1, x2, var.equal=TRUE, alternative="less")
    # t = -16.39, df = 48, p-value < 2.2e-16
    t.test(x1, x3, var.equal=TRUE, alternative="less")
    # t = -21.818, df = 48, p-value < 2.2e-16
    ```

??? info "Pourquoi ne réalisons-nous pas de t-test entre les deux vaccins?"

    Puisque nous sommes au seuil de 0.1%, si p > 0.001, nous acceptons H~0~. Dans les comparaisons multiples, la probabilité critique du test entre les deux vaccins était de 0.01 et donc supérieur au seuil de significativité. Comme nous savons qu'il n'y a donc pas de différence significative entre les deux, nous n'avons pas besoin de tester la direction de la différence.

Nous remarquons donc que les deux vaccins sont significativement plus efficaces que le placebo au seuil de 0.1%.
